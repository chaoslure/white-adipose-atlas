# A single-cell atlas of human and mouse white adipose tissue

See the manuscript, [here][pmid:35296864] (open access link [here][oa]). Genes
can be queried in the dataset using the [Single Cell Portal][scp] and serialized
objects of the final datasets can be found [here][gdrive] (Seurat RDS). Links to
specific clusterings/objects are layed out in the tables below.

> #### Human Data
> | Explore Online (SCP) | Description | Number of Cells | Object Download Link |
> | -------------------- | ----------- | --------------- | -------------------- |
> | [all cells][scp-hs-all] | all human WAT cells | 166,149 | [human_all.rds][gdrive-hs-all] (13.2 GB) |
> | [adipocytes][scp-hs-ads] | adipocyte subclusters | 25,871 | [human_adipocytes.rds][gdrive-hs-ads] (2.3 GB) |
> | [ASPCs][scp-hs-aspcs] | adipose stem and progenitor cell subclusters | 52,482 | [human_ASPCs.rds][gdrive-hs-aspcs] (4.1 GB) |
> | [mesothelial cells][scp-hs-mes] | mesothelial subclusters | 30,482 | [human_mesothelium.rds][gdrive-hs-mes] (1.8 GB) |
> | [vascular cells][scp-hs-vasc] | vascular subclusters | 22,734 | [human_vascular.rds][gdrive-hs-vasc] (1.3 GB) |
> | [immune cells][scp-hs-imm] | immune subclusters | 34,268 | [human_immune.rds][gdrive-hs-imm] (2.7 GB) |
​
> #### Mouse Data
> | Explore Online (SCP) | Description | Number of Cells | Object Download Link |
> | -------------------- | ----------- | --------------- | -------------------- |
> | [all cells][scp-mm-all] | all mouse WAT cells | 197,721 | [mouse_all.rds][gdrive-mm-all] (15.2 GB) |
> | [adipocytes][scp-mm-ads] | adipocyte subclusters | 39,934 | [mouse_adipocytes.rds][gdrive-mm-ads] (3.3 GB) |
> | [ASPCs][scp-mm-aspcs] | adipose stem and progenitor cell subclusters | 51,227 | [mouse_ASPCs.rds][gdrive-mm-aspcs] (3.3 GB) |
> | [mesothelial cells][scp-mm-mes] | mesothelial subclusters | 14,947 | [mouse_mesothelium.rds][gdrive-mm-mes] (550 MB) |
> | [vascular cells][scp-mm-vasc] | vascular subclusters | 7,632 | [mouse_vascular.rds][gdrive-mm-vasc] (503 MB) |
> | [immune cells][scp-mm-imm] | immune subclusters | 70,547 | [mouse_immune.rds][gdrive-mm-imm] (4.2 GB) |

[pmid:35296864]: https://pubmed.ncbi.nlm.nih.gov/35296864/
[oa]: https://rdcu.be/cJXHi
[scp]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue
[scp-hs-all]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue?cluster=Human%20WAT&spatialGroups=--&annotation=cluster--group--study&subsample=all#study-visualize
[scp-hs-ads]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue?cluster=Human%20adipocytes&spatialGroups=--&annotation=subcluster--group--study&subsample=all#study-visualize
[scp-hs-aspcs]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue?cluster=Human%20ASPCs&spatialGroups=--&annotation=subcluster--group--study&subsample=all#study-visualize
[scp-hs-mes]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue?cluster=Human%20mesothelium&spatialGroups=--&annotation=subcluster--group--study&subsample=all#study-visualize
[scp-hs-vasc]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue?cluster=Human%20vascular%20cells&spatialGroups=--&annotation=subcluster--group--study&subsample=all#study-visualize
[scp-hs-imm]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue?cluster=Human%20immune%20cells&spatialGroups=--&annotation=subcluster--group--study&subsample=all#study-visualize
[scp-mm-all]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue?cluster=Mouse%20WAT&spatialGroups=--&annotation=cluster--group--study&subsample=all#study-visualize
[scp-mm-ads]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue?cluster=Mouse%20adipocytes&spatialGroups=--&annotation=subcluster--group--study&subsample=all#study-visualize
[scp-mm-aspcs]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue?cluster=Mouse%20ASPCs&spatialGroups=--&annotation=subcluster--group--study&subsample=all#study-visualize
[scp-mm-mes]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue?cluster=Mouse%20mesothelium&spatialGroups=--&annotation=subcluster--group--study&subsample=all#study-visualize
[scp-mm-vasc]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue?cluster=Mouse%20vascular%20cells&spatialGroups=--&annotation=subcluster--group--study&subsample=all#study-visualize
[scp-mm-imm]: https://singlecell.broadinstitute.org/single_cell/study/SCP1376/a-single-cell-atlas-of-human-and-mouse-white-adipose-tissue?cluster=Mouse%20immune%20cells&spatialGroups=--&annotation=subcluster--group--study&subsample=all#study-visualize
[gdrive]: https://drive.google.com/drive/folders/1rZ6Cnivacb9ispdndNngB9VgZsnYmVop?usp=sharing
[gdrive-hs-all]: https://drive.google.com/file/d/1ReTOuHsBHyW0TZ2D9mVdTzxAV0npyV7G/view?usp=sharing
[gdrive-hs-ads]: https://drive.google.com/file/d/1z2LLH9ZoH5zmCvkjwVJzaBJPDUv45eTQ/view?usp=sharing
[gdrive-hs-aspcs]: https://drive.google.com/file/d/1uEWzVXASWQu-jtxelj1V7VK-3XYVwSUH/view?usp=sharing
[gdrive-hs-mes]: https://drive.google.com/file/d/1FR-_BlXRiVvE3Hzr1PDnxT7XHOL5jL07/view?usp=sharing
[gdrive-hs-vasc]: https://drive.google.com/file/d/1PB3tJFueXLkKSC7Q7VwWfKBceoDeToat/view?usp=sharing
[gdrive-hs-imm]: https://drive.google.com/file/d/1PB3tJFueXLkKSC7Q7VwWfKBceoDeToat/view?usp=sharing
[gdrive-mm-all]: https://drive.google.com/file/d/1az-DH7uIpg6NPgUKa8qhDeV5rFnlop02/view?usp=sharing
[gdrive-mm-ads]: https://drive.google.com/file/d/1xUr_daZ70MI1ZKnMnDSOFeSmGCRzRqY0/view?usp=sharing
[gdrive-mm-aspcs]: https://drive.google.com/file/d/1-qfIJGD_CyL9A5xlCCDGNtrydV2CMcKL/view?usp=sharing
[gdrive-mm-mes]: https://drive.google.com/file/d/1jxKhCCYeEYscmuBwvjF_0ea1noxfNjQ5/view?usp=sharing
[gdrive-mm-vasc]: https://drive.google.com/file/d/1BdYULEgWoNpiwgsiyg6E9CWPzdCreKhr/view?usp=sharing
[gdrive-mm-imm]: https://drive.google.com/file/d/1Wy0CqBkFYE-aEz1wVSqTgQ42OWdfsrDn/view?usp=sharing

## <a name="table-of-contents"></a>Table of contents

   1. [Initialization](#init)
<!--       1. [Installing prerequisite software](#install) -->
<!--       2. [Downloading and/or generating common resources](#setup) -->
<!--    2. [Raw data processing](#raw) -->
<!--       1. [Processing the human Drop-Seq data](#hscrna) -->
<!--       2. [Processing the *human* Chromium SC3P (v3) data](#hsnuc) -->
<!--       3. [Processing the *mouse* Chromium SC3P (v3) data](#msnuc) -->
   3. [Single-cell analyses](#sca)
      1. [Doublet scoring/prediction](#scrublet)
      2. [Clustering and annotation](#seurat)
<!--       2. [CELLECT and such](#cellect) -->

## <a name="init"></a>Initialization

Prerequisite software:

   - [curl][] (or similar, e.g. [Wget][])
   - [Python](https://www.python.org/) >= 3.7
<!--      - [CellBender][]: version  -->
     - [scrublet][]: version 0.2.1
   - [R](https://www.r-project.org/): version >= 4.0
     - [Matrix](https://www.r-pkg.org/pkg/Matrix): version 1.4-0
     - [tidyverse][]: 1.3.1 (see also: <https://www.tidyverse.org/>)
     - [Seurat][]: version 4.0.0 (see also: <https://satijalab.org/seurat/>)
     - [LIGER][]: version 0.5.0
     - [yaml](https://www.r-pkg.org/pkg/yaml): version 2.3.5
     - [gridExtra](https://www.r-pkg.org/pkg/gridExtra): version 2.3
     - [ggExtra](https://www.r-pkg.org/pkg/ggExtra): version 0.9
     - [riverplot](https://www.r-pkg.org/pkg/riverplot): version 0.10
     - [gplots](https://www.r-pkg.org/pkg/gplots): version 3.1.1
     - various [Bioconductor] packages: version 3.11
       - [org.Hs.eg.db](https://bioconductor.org/packages/3.11/data/annotation/html/org.Hs.eg.db.html)
       - [org.Mm.eg.db](https://bioconductor.org/packages/3.11/data/annotation/html/org.Mm.eg.db.html)
       - [clusterProfiler](https://bioconductor.org/packages/3.11/bioc/html/clusterProfiler.html)
       - [DOSE](https://bioconductor.org/packages/3.11/bioc/html/DOSE.html)
<!--    - [CELLECT][]: version 1.1.0 -->

[curl]: https://curl.se/
[wget]: https://www.gnu.org/software/wget/
[cellbender]: https://github.com/broadinstitute/CellBender
[scrublet]: https://github.com/swolock/scrublet
[tidyverse]: https://www.r-pkg.org/pkg/tidyverse
[seurat]: https://www.r-pkg.org/pkg/Seurat
[liger]: https://github.com/welch-lab/liger
<!-- [cellect]: https://github.com/perslab/CELLECT -->

## <a name="sca"></a>Single-cell analyses
The clustering and annotation scripts expect gene-by-cell count matrices (either
compressed or not) as input. The raw data processing pipeline steps above will
regenerate these count matrices, but are time- and resource-consuming processes.
As an alternative, the count matrices are made available in public repositories
and can be easily downloaded and distributed to the default locations assumed by
the sub-/clustering and annotation scripts described below. The following code
snippets will do this for the Drop-Seq and 10X data, respectively (requires:
[`curl`][curl]).

```sh
curl -L 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE176067&format=file' -o GSE176067.tar
tar --extract --file GSE176067.tar
for file in GSM*_Hs_SAT_SVF*.dge.tsv.gz; do mv -v "$file" "./samples/$(basename "${file#GSM[[:digit:]]*_}" .dge.tsv.gz)/dge.tsv.gz"; done
```

```sh
curl -L 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE176171&format=file' -o GSE176171.tar
tar --extract --file GSE176171.tar
for file in GSM*_Hs_[OS]AT_[[:digit:]]*.dge.tsv.gz; do mv -v "$file" "./samples/$(basename "${file#GSM[[:digit:]]*_}" .dge.tsv.gz)/dge.tsv.gz"; done
for file in GSM*_Mm_{ING,EPI,POV}*.dge.tsv.gz; do mv -v "$file" "./samples/$(basename "${file#GSM[[:digit:]]*_}" .dge.tsv.gz)/dge.tsv.gz"; done
```

### <a name="scrublet"></a>Doublet scoring/prediction
Doublet cells/nuclei were predicted using [scrublet][]. The following script
will re-generate the doublet scores and predictions:

```
python3 ../scripts/scrub.py
```

### <a name="seurat"></a>Clustering and annotation
The clustering script `./scripts/cluster.r` expects a single configuration file
as input. Such configuration files are YAML files that describe the sample
inputs (paths to count matrices and explicit associated metadata) and other
pipeline parameters, such as cutoff thresholds for filtering cells and genes
(min. expr., max. mito. expr., etc.), the number of PCs and the clustering
resolution, as well as the integration method. They may also specify lists of
genes to plot across various visualizations (violin plots, dot plots, feature
plots). The gene list included in each of the configuration scripts here are a
curated list of marker genes for cell populations that can help identify mixed
(or doublet/multiplet) clusters.

The paper clusterings were obtained by first clustering all cells together, for
each species separately. Clusters were annotated and major subpopulations were
subsequently subclustered (at least) twice: primarily to identify subpopulations
to be marked for removal (e.g. doublet populations or mislabeled cells from the
all cell level), and lastly to obtain an atlas clustering for annotation of cell
types and subtypes. Annotations are assigned on the sub-clusterings and labels
are transferred/propagated back up to the all-cell objects.

The following snippets will run the clustering, subclustering, and annotation
scripts. These scripts are resource (including time) intensive. It is generally
advisable to submit these scripts to an HPC cluster. Particularly, the all-cell
objects require upwards of 64GB of memory and will run over several hours. The
initial subclusterings require at least enough memory to be able to read in the
all-cell objects to subset out the required clusters for further analyses. It
should suffice to provide btw. 32-64GB of memory in these cases. These scripts
are (unforutnately) not optimized for running in parallel; of course, the two
species and all of the subclusterings may be run/submitted separately.

```
Rscript ./scripts/cluster.r ./seurat/human/all/pipeconfig.yaml
Rscript ./scripts/subcluster.r ./seurat/human/sub/pipeconfig.yaml
Rscript ./scripts/subcluster.r ./seurat/human/sub/preads2/pipeconfig.yaml
for cfg in ./seurat/human/{ads,preads,mes,vasc,imm}/pipeconfig.yaml; do Rscript ./scripts/subcluster.r "$cfg"; done
for cfg in ./seurat/human/imm/{macs,tnk,bcell}/pipeconfig.yaml; do Rscript ./scripts/subcluster.r "$cfg"; done
Rscript ./seurat/human/annotate.r
```

```
Rscript ./scripts/cluster.r ./seurat/mouse/all/pipeconfig.yaml
Rscript ./scripts/subcluster.r ./seurat/mouse/sub/pipeconfig.yaml
Rscript ./scripts/subcluster.r ./seurat/mouse/sub/{ads,preads,vasc}2/pipeconfig.yaml
Rscript ./scripts/subcluster.r ./seurat/mouse/sub/ads3/pipeconfig.yaml
for cfg in ./seurat/mouse/{ads,preads,mes,vasc,imm}/pipeconfig.yaml; do Rscript ./scripts/subcluster.r "$cfg"; done
for cfg in ./seurat/mouse/imm/{macs,monos,tnk,bcell}/pipeconfig.yaml; do Rscript ./scripts/subcluster.r "$cfg"; done
Rscript ./seurat/mouse/annotate.r
```

<!-- ### <a name="cellect"></a>CELLECT and such -->

